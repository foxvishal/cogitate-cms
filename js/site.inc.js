// JavaScript Document

$(document).ready(function(){
    if($('.careerSlider').length > 0){
        $('.careerSlider').slick({
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: false,
        });
    }

    if($('.commissionLOB .innerLOB').length > 0){
        $('.commissionLOB .innerLOB').slick({
            infinite: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            responsive: [
            {
                breakpoint: 1100,
                settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                infinite: false,
                }
            }
            ]
        });
    }

    $("body").click(function(event ){
        var $target = $(event.target);
        if(!$target.is(".filterInner *")){
            if($('.tableWrap .tableOptions .filterDiv .filterInner').hasClass('active')){
                $(".tableWrap .tableOptions .filterDiv .filterInner ul").slideUp('medium');
                $('.tableWrap .tableOptions .filterDiv .filterInner').removeClass('active');
            }
        }
    }); 

    $(".tableWrap .tableOptions .filterDiv .filterInner a").click(function(){
        $(this).parent().toggleClass('active');
        $(".tableWrap .tableOptions .filterDiv .filterInner ul").slideToggle('medium');
        return false;
    });

    $(".folderList ul li").click(function(){
        if(!$(this).hasClass('open')){
            $(".folderList ul li").removeClass('open');
            $(this).addClass('open');
        }else{
            $(this).removeClass('open');
        }
    });

    $(".documentList ul li").click(function(){
        if(!$(this).hasClass('open')){
            $(".documentList ul li").removeClass('open');
            $(this).addClass('open');
        }else{
            $(this).removeClass('open');
        }
    });

    if($('.custom-select').length > 0){
        $('.custom-select').select2({
            minimumResultsForSearch: Infinity
        });

        
    }

    if($(".datepicker").length > 0 || $(".dateRangepicker").length > 0){
        setTimeout(function(){ 
            $('.flatpickr-monthDropdown-months').select2({
                minimumResultsForSearch: Infinity
            });
        }, 1000);
    }

    if($('.popup-with-form').length > 0){
        $('.popup-with-form').magnificPopup({
            type: 'inline',
            preloader: false,
            focus: '#name',
            closeOnBgClick: false,
            callbacks: {
                beforeOpen: function() { 
                    $('body').css('overflow','hidden'); 
                },
                close: function() { 
                    $('body').css('overflow','auto'); 
                }
            }
        });
    }

    $("#popFrm").submit(function(){
        $.magnificPopup.close();
        $.magnificPopup.open({
            items: {
              src: '#thankPop'
            },
            type: 'inline',
            closeOnBgClick: false,
            showCloseBtn: false,
            callbacks: {
                beforeOpen: function() { 
                    $('body').css('overflow','hidden'); 
                },
                close: function() { 
                    $('body').css('overflow','auto'); 
                }
            }
        }, 0);
    });

    $(".thankPop .popInner a").click(function(){
        $.magnificPopup.close();
        return false;
    });
      
});

$( function() {
    //alert($(window).width());

    if($(".datepicker").length > 0){
        var dpickr = $(".datepicker").flatpickr();
        $(".profileWrap ul li .fieldDiv .fieldSub1 i").click(function(){
            dpickr.toggle();
        });
    }

    if($(".dateRangepicker").length > 0){
        var picker = $(".dateRangepicker").flatpickr({
            mode: "range",
            dateFormat: "d M Y",
            defaultDate: ["21 mar 2019", "30 apr 2019"]
        });

        $(".reportrange i").click(function(){
            picker.open();
        });
    }

    if($(".dateRangepicker2").length > 0){
        var picker2 = $(".dateRangepicker2").flatpickr({
            mode: "range",
            dateFormat: "d M Y",
            defaultDate: ["21 mar 2019", "30 apr 2019"]
        });

        $(".reportrange2 i").click(function(){
            picker2.open();
        });
    }

    $(".tropen").click(function(){
        var thistr = $(this).closest("tr");
        if(!thistr.hasClass('active')){
            $("table.recentDoc.contactDoc tr").removeClass('active');
            $("table.recentDoc.contactDoc tr.p").hide();
            $(this).closest("tr").addClass('active');
            $(this).closest("tr").next().show();
        }else{
            $("table.recentDoc.contactDoc tr").removeClass('active');
            $("table.recentDoc.contactDoc tr.p").hide();
        }
        
        return false;
    });
});