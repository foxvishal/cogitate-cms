// JavaScript Document

$(document).ready(function(){
    if($('.careerSlider').length > 0){
        $('.careerSlider').slick({
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: false,
        });
    }

    if($('.commissionLOB .innerLOB').length > 0){
        $('.commissionLOB .innerLOB').slick({
            infinite: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            responsive: [
            {
                breakpoint: 1100,
                settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                infinite: false,
                }
            }
            ]
        });
    }

    $("body").click(function(event ){
        var $target = $(event.target);
        if(!$target.is(".filterInner *")){
            if($('.tableWrap .tableOptions .filterDiv .filterInner').hasClass('active')){
                $(".tableWrap .tableOptions .filterDiv .filterInner ul").slideUp('medium');
                $('.tableWrap .tableOptions .filterDiv .filterInner').removeClass('active');
            }
        }
    }); 

    $(".tableWrap .tableOptions .filterDiv .filterInner a").click(function(){
        $(this).parent().toggleClass('active');
        $(".tableWrap .tableOptions .filterDiv .filterInner ul").slideToggle('medium');
        return false;
    });

    $(".folderList ul li").click(function(){
        if(!$(this).hasClass('open')){
            $(".folderList ul li").removeClass('open');
            $(this).addClass('open');
        }else{
            $(this).removeClass('open');
        }
    });

    $('.custom-select').select2();
});

$( function() {
    if($(".datepicker").length > 0){

        $(".datepicker").flatpickr();
    }

    $(".tropen").click(function(){
        var thistr = $(this).closest("tr");
        if(!thistr.hasClass('active')){
            $("table.recentDoc.contactDoc tr").removeClass('active');
            $("table.recentDoc.contactDoc tr.p").hide();
            $(this).closest("tr").addClass('active');
            $(this).closest("tr").next().show();
        }else{
            $("table.recentDoc.contactDoc tr").removeClass('active');
            $("table.recentDoc.contactDoc tr.p").hide();
        }
        
        return false;
    });
});

$(function() {
    
    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('D MMM YYYY') + ' - ' + end.format('D MMM YYYY'));
    }


    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);



    var start2 = moment().subtract(29, 'days');
    var end2 = moment();

    function cb2(start2, end2) {
        $('#reportrange2 span').html(start2.format('D MMM YYYY') + ' - ' + end2.format('D MMM YYYY'));
    }

    $('#reportrange2').daterangepicker({
        startDate: start2,
        endDate: end2,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb2);

    cb2(start2, end2);

});